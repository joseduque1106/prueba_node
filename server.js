const express = require('express');
const bodyParser = require('body-parser');
const mongoose = require('mongoose');


const app = express();

//configuracion globlal de rutas
app.use(require('./routes/index'));

mongoose.connect('mongodb://localhost/prueba', (err, res) => {
    if (err) throw err;
    console.log("Base de datos online");
});

app.listen(3000, () => {
    console.log("escuchando peticiones por el puerto 3000");
});