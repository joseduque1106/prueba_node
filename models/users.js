// ruta para conexion con la db
const mongoose = require('mongoose');
const uniqueValidator = require('mongoose-unique-validator');

let Schema = mongoose.Schema;

let userSchema = new Schema({
    nombre: { type: String, required: [true, 'el nombre es necesario'] },
    cedula: { type: String, required: [true, 'la cedula es necesaria'] },
    celular: { type: String, required: [true, 'el celluar es necesario es necesaria'] },
    email: { type: String, unique: true, required: [true, 'el correo es obligatorio'] },
    password: { type: String, required: [true, 'el correo es obligatorio'] }

});


userSchema.plugin(uniqueValidator, {
    message: '{PATH} debe de ser unico'
})
module.exports = mongoose.model('Usuario', userSchema);