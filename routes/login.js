const express = require('express');
const bcrypt = require('bcrypt');

const Usuario = require('../models/users');

const app = express();

app.post('/login', (req, res) => {

    let body = req.body;

    usuario.findOne({ email: body.email }, (err, usuarioDB) => {

        if (err) {
            return res.json({
                ok: false,
                err
            })
        }

        if (usuarioDB == null) {
            return res.json({
                ok: true,
                mensaje: "Usuario no existe en la Base de datos"
            });
        } else {
            if (!bcrypt.compareSync(body.password, usuarioDB.password)) {
                return res.json({
                    ok: true,
                    mensaje: "Contraseña errónea"
                })
            }

            return res.status(200).json({
                ok: true,
                token
            });


        }

    });

});

module.exports = app;